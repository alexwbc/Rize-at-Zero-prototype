extends Node

const db_files = [{"string":"config","file":"res://system/config.json"}
#				,{"string":"string_name","file":"res://file/json/path.json"}
]



#database
var config#db_files will load the content in there



#nodes
var lobby#lobby will place itself here (/lobby.tscn)
var world#

# Default game port
const DEFAULT_PORT = 10567

# Max number of players
const MAX_PEERS = 12

# Name for my player
var player_name = "Riser"

# Names for remote players in id:name format
var players = {}

# Signals to let lobby GUI know what's going on
signal player_list_changed()
signal connection_failed()
signal connection_succeeded()
signal game_ended()
signal game_error(what)



#dumy and testing variables
var player_target_test = null

func _enter_tree():

	for i in db_files:#loading database
		var file = File.new()
		file.open(i["file"], file.READ)
		set(i["string"], parse_json(file.get_as_text()))
		file.close()
	print("load lobby")
	if config["server"]:
		lobby = load("res://server/server_lobby.tscn").instance()
	else:
		lobby = load("res://client/client_lobby.tscn").instance()



# Callback from SceneTree
func _player_connected(id):
	# This is not used in this demo, because _connected_ok is called for clients
	# on success and will do the job.
	pass

# Callback from SceneTree
func _player_disconnected(id):
	if get_tree().is_network_server():
		if has_node("/root/world"): # Game is in progress
			emit_signal("game_error", "Player " + players[id] + " disconnected")
			end_game()
		else: # Game is not in progress
			# If we are the server, send to the new dude all the already registered players
			unregister_player(id)
			for p_id in players:
				# Erase in the server
				rpc_id(p_id, "unregister_player", id)

# Callback from SceneTree, only for clients (not server)
func _connected_ok():
	# Registration of a client beings here, tell everyone that we are here
	rpc("register_player", get_tree().get_network_unique_id(), player_name)
	emit_signal("connection_succeeded")

# Callback from SceneTree, only for clients (not server)
func _server_disconnected():
	emit_signal("game_error", "Server disconnected")
	end_game()

# Callback from SceneTree, only for clients (not server)
func _connected_fail():
	get_tree().set_network_peer(null) # Remove peer
	emit_signal("connection_failed")

# Lobby management functions

remote func register_player(id, new_player_name):
	if get_tree().is_network_server():
		# If we are the server, let everyone know about the new player
		rpc_id(id, "register_player", 1, player_name) # Send myself to new dude
		for p_id in players: # Then, for each remote player
			rpc_id(id, "register_player", p_id, players[p_id]) # Send player to new dude
			rpc_id(p_id, "register_player", id, new_player_name) # Send new dude to player

	players[id] = new_player_name
	emit_signal("player_list_changed")

remote func unregister_player(id):
	players.erase(id)
	emit_signal("player_list_changed")

remote func pre_start_game(spawn_points):
	gamestate.lobby.hide()
	
	var logic = Node.new()
	if get_tree().is_network_server():
		world = load("res://system/server_world.tscn").instance()#no material, no stuff: only basic collision
		logic.set_script(load("res://server/world_logic.gd"))
	else:
		world = load("res://system/client_world.tscn").instance()
		logic.set_script(load("res://client/world_logic.gd"))
	logic.world = world
	logic.name = "logic"
	world.add_child(logic)
#



	var player_roster = Node.new()
	player_roster.name = "players"
	world.add_child(player_roster)
	player_roster.set_owner(world)
	
	get_tree().get_root().add_child(world)
	gamestate.lobby.hide()
	var world_spawn = []
#	for spawn in world.get_node("pos")
#	print(world.spawn_list)

#	world.get_node("players").add_child(player)

	var spawn_count = 0

	for p_id in spawn_points:
		var spawn_pos = world.spawn_list[spawn_count]
		if spawn_count >= world.spawn_list.size():#at the end of spawnlist, return to 0 (restart)
			spawn_count = 0
		else:
			spawn_count +=1
		var player = load("res://player/player_frame.tscn").instance()
		#mode onserver,vehicle,ghost
		if get_tree().is_network_server():#server spawn onlyPFOS (player on server)
			player.mode = "onserver"
		elif p_id == get_tree().get_network_unique_id():#client place it's own vehicle
			player.mode = "vehicle"
		else:#anything else (on client), are just ghosts
			player.mode = "ghost"
			
		player.set_name(str(p_id))

		player.spawn_pos=spawn_pos


		world.get_node("players").add_child(player)



remote func post_start_game():
	get_tree().set_pause(false) # Unpause and unleash the game!

var players_ready = []

remote func ready_to_start(id):
	assert(get_tree().is_network_server())

	if not id in players_ready:
		players_ready.append(id)

	if players_ready.size() == players.size():
		for p in players:
			rpc_id(p, "post_start_game")
		post_start_game()

func host_game(new_player_name):
	player_name = new_player_name
	var host = NetworkedMultiplayerENet.new()
	host.create_server(DEFAULT_PORT, MAX_PEERS)
	get_tree().set_network_peer(host)

func join_game(ip, new_player_name):
	player_name = new_player_name
	var host = NetworkedMultiplayerENet.new()
	host.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(host)

func get_player_list():
	return players.values()

func get_player_name():
	return player_name

func begin_game():
	assert(get_tree().is_network_server())

	# Create a dictionary with peer id and respective spawn points, could be improved by randomizing
	var spawn_points = {}
#	spawn_points[1] = 0 # Server in spawn point 0#ONGOING: server is not player
	var spawn_point_idx = 1
	for p in players:
		spawn_points[p] = spawn_point_idx
		spawn_point_idx += 1
	# Call to pre-start game with the spawn points
	for p in players:
		rpc_id(p, "pre_start_game", spawn_points)

	pre_start_game(spawn_points)

func trigger_swarm(cord):
	if !get_tree().is_network_server():
		return
	
	var spawn_point_idx = 1	
	for p in players:
		rpc_id(p, "introduce_swarm", cord)
		spawn_point_idx += 1

	introduce_swarm(cord)

remote func introduce_swarm(cord):
#	print(str("coord is: ",cord))
#	var template = world.get_node("logic")
	var new_swarm = world.get_node("logic").swarm_template.instance()#from world logic, take the swarm teplate (frame)
	var logic_swarm = Node.new()#this will contain the logic for the swarm
	if get_tree().is_network_server():
#		print("set server logic")
		logic_swarm.set_script(load("res://elements/swarm_server.gd"))#the logic is for the server
	else:
#		print("set client logic")
		logic_swarm.set_script(load("res://elements/swarm_client.gd"))#the logic is for the client
	new_swarm.call_deferred("add_child", logic_swarm)
	world.call_deferred("add_child", new_swarm)
#	world.add_child(new_swarm)
	new_swarm.pos = cord
#	print(world.swarm_height)
#	print("node get swarm creation")
	
func end_game():
	if has_node("/root/world"): # Game is in progress
		# End it
		get_node("/root/world").queue_free()

	emit_signal("game_ended")
	players.clear()
	get_tree().set_network_peer(null) # End networking

func _ready():
	add_child(lobby)

	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
