extends Node

#nodes
var frame

func _enter_tree():
	frame = get_parent()

#	frame.global_transform.origin = frame.pos


func _process(delta):
	var current_pos = frame.global_transform.origin
	
#	var current_pos = global_transform.origin
#	print("pos is: " +str(pos))
	if frame.pos != current_pos:
		frame.global_transform.origin = current_pos.linear_interpolate(frame.pos, 0.2)
		