extends Node

const fly_speed = Vector3(4,4,4)
const fly_slowed = Vector3(1,1,1)

#nodes
var frame

var waypoints = []
var current_waypoint = 1


var sync_delay = 0.03
var sync_count = 0


var target

#moving logic
var slowdown = false# something is slowing down the swarm



#debug
var body

func process_cruise(delta):
	var position = frame.global_transform.origin
	var dif = position-waypoints[current_waypoint]
	frame.global_transform.origin = position.linear_interpolate(waypoints[current_waypoint], .01)
	if dif.abs() < Vector3(1,1,1):
		if current_waypoint == (waypoints.size()-1):
			current_waypoint = 0
		else:
			current_waypoint += 1
	if gamestate.player_target_test != null:
		frame.status = frame.status_db["hunt"]
		target = gamestate.player_target_test


func process_hunt(delta):#the swarm behave more like a ghost, so there's no real need to use real physics to move it
	if target == null:
		abandon_chase()
	#pos is frame.pos
	var target_pos = target.global_transform.origin
	frame.look_at(target_pos, Vector3(0,1,0))
	var direction = frame.pos-target_pos
	if !slowdown:
		direction = direction.normalized()*fly_speed*delta
	else:
		direction = direction.normalized()*fly_slowed*delta
		
#	print(str("dist: ", distance, " direction: ", distance.normalized()))
#	frame.global_transform.origin = frame.pos.linear_interpolate(target_pos,delta)
	frame.global_transform.origin -= direction

func _physics_process(delta):
	if frame.status == frame.status_db["hunt"]:
		phy_hunt(delta)


func phy_hunt(delta):
	pass
#	if target == null:
#		abandon_chase()
#	var dir = frame.transform.basis.x
#	frame.move_and_slide(dir * 1)
#	var dir = onserver.dir
##	dir.y = 0
#	dir = dir.normalized()
#
#	vel.y += delta * g
#
#	var hvel = vel
#	hvel.y = 0
#
#	var target = dir * MAX_SPEED
#	var accel
#	if dir.dot(hvel) > 0:
#		accel = ACCEL
#	else:
#		accel = DEACCEL
#
#	hvel = hvel.linear_interpolate(target, accel * delta)
#
#	vel.x = hvel.x
#	vel.z = hvel.z
#
#	#	vel = move_and_slide(vel, Vector3(0,1,0))
#	vel = onserver.move_and_slide(vel, Vector3(0,1,0))
#



func proc_col(objects, delta):#swarm is colliding with something
	slowdown = true
#	var ob_list = []
#	for i in objects:
#		ob_list.append(i.name)
#	print(ob_list)
	



func _process(delta):
	slowdown = false
#	var collision = frame.get_overlapping_bodies()
	var collision = frame.get_overlapping_areas()
	
	if collision.size() != 0:
		proc_col(collision, delta)
#	var collision = frame.get_overlapping_bodies() 
#	var collision = frame.get_overlapping_areas()
#	print(body.name)
#	print(str("body: ",frame.get_overlapping_bodies().size()," areas: ",frame.get_overlapping_areas().size()))
#	print(str("colliding with: ",frame.get_overlapping_bodies().size()," bodies"))
#	if collision == null:
#		pass
##		print("colliding with something")
	if frame.status == frame.status_db["cruise"]:
		process_cruise(delta)
	elif frame.status == frame.status_db["hunt"]:
		process_hunt(delta)
#
#
	if sync_count < 0:

		frame.rset_unreliable("pos", frame.global_transform.origin)
#		pos = global_transform.origin
#
		sync_count = sync_delay
	else:
		sync_count -= delta

func _enter_tree():
	body = get_parent().get_node("CollisionShape/body")
	frame = get_parent()
	get_sample_waypoints()
	frame.global_transform.origin = frame.pos



func get_sample_waypoints():
	waypoints = []
	for wp in gamestate.world.get_node("temp_waypoints").get_children():
		waypoints.append(wp.global_transform.origin)
	print(waypoints)

func abandon_chase():
	print("abandon chase")
	frame.status = frame.status_db["cruise"]