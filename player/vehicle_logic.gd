extends Node
#player-frame on client (PFVeichle): like a car, player can give command..
#but player is no responsable for the actual action. Like in a car, when
#you steer or accellerate, it's not your muscle that directly affect
#car motion. Engine and gears are elsewhere (the server).
#Player in PFVeichle push button, the server get the button pressed signal
#and move PFoS accordly, the server process the speed/action and influx
#and finally update back the PFVeichle. PFVeichle (freezed until now)
#finally moves.
var vehicle


#TODO
#sent input to server
		#walk direction
		#button action
		#button run
#nodes
var camera
var base_camera_pos
var wire_cast#ray cast for TP camera collision("wire" between camera and head position)
var yaw_arm#yaw's node
var pitch_arm#pitch's node
var head


#temporary and debug variables
var anim_node
var old_anim


#var input_map_db = {
var input_map = [false,false,false,false]
#var input_map_ctrl


var yaw = 0
var pitch = 0

var mouse_speed = 0.2
var mouse_motion = Vector2(0,0)


const pitch_limit = Vector2(-0.3,0.7)


func _enter_tree():
	vehicle = get_parent()
	yaw_arm = vehicle.get_node("yaw")
	pitch_arm = vehicle.get_node("yaw/pitch")
	camera = vehicle.get_node("yaw/pitch/camera")

	wire_cast = vehicle.get_node("wire")
	wire_cast.add_exception(self)
	head = vehicle.get_node("headpos")
	base_camera_pos = vehicle.get_node("yaw/pitch/base_camerapos")
#debug&dev sets
	anim_node = vehicle.get_node("bodymesh/character/AnimationPlayer")



func _ready():
	set_physics_process(true)


func _input(event):
	if event.get_class() == "InputEventMouseMotion":
		mouse_motion = event.relative





func normal_process(delta):
	var vel = Vector3()
	vel = (vehicle.global_transform.origin-vehicle.pos).abs()
	vel = vel.x+vel.z
	var curr_anim = "idle"
	if vel > 0.1:
		curr_anim = "run"


	if curr_anim != old_anim:
		print(str("changing anim normal: ", curr_anim))
		anim_node.play(curr_anim)
		
	old_anim = curr_anim



func stealth_process(delta):
	var vel = Vector3()
	vel = (vehicle.global_transform.origin-vehicle.pos).abs()
	vel = vel.x+vel.z
	var curr_anim = "stealth"
	if vel > 0.1:
		curr_anim = "stealth-walk"


	if curr_anim != old_anim:
		print(str("changing anim stealth: ", curr_anim))
		anim_node.play(curr_anim)
		
	old_anim = curr_anim





func _process(delta):
	#ghost process
	vehicle.get_node("Label").text = str("my name: ",vehicle.name,"\nmy loc: ", vehicle.pos)
	var pos = vehicle.transform.origin
	if pos != vehicle.pos:
		vehicle.transform.origin = pos.linear_interpolate(vehicle.pos,0.1)
	
	yaw -= mouse_motion.x*mouse_speed*delta
	pitch += mouse_motion.y*mouse_speed*delta
	pitch =  min(max(pitch_limit.x,pitch), pitch_limit.y)
	yaw_arm.rotation.y = yaw
	pitch_arm.rotation.x = pitch
	camera.look_at(head.global_transform.origin, Vector3(0,1,0))
	mouse_motion = Vector2(0,0)


	if vehicle.status == vehicle.status_db["normal"]:
		normal_process(delta)
	if vehicle.status == vehicle.status_db["stealth"]:
		stealth_process(delta)



	input_map[vehicle.input_map_db["jump"]] = Input.is_action_pressed("jump")
	input_map[vehicle.input_map_db["stealth"]] = Input.is_action_pressed("stealth")
	input_map[vehicle.input_map_db["fast_travel"]] = Input.is_action_pressed("fast_travel")
	input_map[vehicle.input_map_db["scavenge"]] = Input.is_action_pressed("scavenge")





	if vehicle.input_map != input_map:
		print("change detectd, sync")
		vehicle.input_map = input_map.duplicate(true)
		vehicle.rset_id(1, "input_map", vehicle.input_map)

func _physics_process(delta):
	var cameraplace = base_camera_pos.global_transform.origin
	var headplace = head.global_transform.origin
	wire_cast.global_transform.origin = headplace
	wire_cast.cast_to = cameraplace-headplace#extract local cordinate from global


	if wire_cast.is_colliding():#when wirecast collide, we get the camera closer to character
		vehicle.get_node("yaw/pitch/camerapos").global_transform.origin = wire_cast.get_collision_point()
		camera.speed = 10
	else :
		vehicle.get_node("yaw/pitch/camerapos").global_transform.origin = cameraplace
		camera.speed = 1



	var dir = Vector3()
	var cam_xform = camera.get_global_transform()

	if Input.is_action_pressed("move_forward"):
		dir += -cam_xform.basis[2]
	if Input.is_action_pressed("move_backward"):
		dir += cam_xform.basis[2]
	if Input.is_action_pressed("move_left"):
		dir += -cam_xform.basis[0]
	if Input.is_action_pressed("move_right"):
		dir += cam_xform.basis[0]
	vehicle.rset("dir", dir)
#	print(dir)


	#orientation
	var pos = vehicle.global_transform.origin
	var vel = Vector3()
	if pos != vehicle.pos:
		vehicle.global_transform.origin = pos.linear_interpolate(vehicle.pos,0.1)
		var dif = vehicle.pos-pos
		vehicle.get_node("bodymesh").rotation.y = atan2(dif.x, dif.z)
		
#	test_animation(delta)#temporary function to control animations (the server will do this)


#func test_animation(delta):#this function will be erased, it's not up to the ghost to change animation state
##							#ghost, instead, will execute the animation modes the server order to.
#
##	var vel = Vector3(vehicle.global_transform.origin-vehicle.pos).abs()
#	var vel = Vector3()
#	vel = (vehicle.global_transform.origin-vehicle.pos).abs()
#	vel = vel.x+vel.z
#	var curr_anim = "idle"
#	if vel > 0.1:
#		curr_anim = "run"
#
#
#	if curr_anim != old_anim:
#		print(str("changing anim: ", curr_anim))
#		anim_node.play(curr_anim)
#
#	old_anim = curr_anim
##
#



