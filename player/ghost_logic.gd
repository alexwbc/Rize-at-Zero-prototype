extends Node
#player-frame on remote (PFGhost): this is what each player see about other
#players (everyone but their own, which is a separate entity). Runs only on
#client, and only receive update from the server. Server don't actually care
#about these entities, only care to give updated data

var ghost

var cur_pos = Vector3()
var old_pos = Vector3()

const turn_speed = 7#0= unstant, 0.5 half, 1 slow


#temporary and debug variables
var anim_node
var old_anim



func _enter_tree():
	
	ghost = get_parent()

#debug&dev sets
	anim_node = ghost.get_node("bodymesh/character/AnimationPlayer")


func _ready():
	print("ghost logic initiated")
	set_physics_process(true)



func normal_process(delta):
	var vel = Vector3()
	vel = (ghost.global_transform.origin-ghost.pos).abs()
	vel = vel.x+vel.z
	var curr_anim = "idle"
	if vel > 0.1:
		curr_anim = "run"


	if curr_anim != old_anim:
		print(str("changing anim normal: ", curr_anim))
		anim_node.play(curr_anim)
		
	old_anim = curr_anim



func stealth_process(delta):
	var vel = Vector3()
	vel = (ghost.global_transform.origin-ghost.pos).abs()
	vel = vel.x+vel.z
	var curr_anim = "stealth"
	if vel > 0.1:
		curr_anim = "stealth-walk"


	if curr_anim != old_anim:
		print(str("changing anim stealth: ", curr_anim))
		anim_node.play(curr_anim)
		
	old_anim = curr_anim



func _process(delta):
	
	if ghost.status == ghost.status_db["normal"]:
		normal_process(delta)
	if ghost.status == ghost.status_db["stealth"]:
		stealth_process(delta)

func _physics_process(delta):
	var pos = ghost.global_transform.origin
	var vel = Vector3()
	if pos != ghost.pos:
		ghost.global_transform.origin = pos.linear_interpolate(ghost.pos,0.1)
		var dif = ghost.pos-pos
		ghost.get_node("bodymesh").rotation.y = atan2(dif.x, dif.z)
		
#		ghost.global_transform.origin = pos.linear_interpolate(ghost.pos,0.1)
#		vel = pos-ghost.pos
#		var angle = atan2(ghost.dir.x, ghost.dir.z )
#		if angle != 0:
#
#			var rot = ghost.get_node("bodymesh").rotation
#			ghost.get_node("bodymesh").rotation = rot.slerp(angle, turn_speed*delta)
#
#	test_animation(delta)



#
#
#
#func test_animation(delta):#this function will be erased, it's not up to the ghost to change animation state
##							#ghost, instead, will execute the animation modes the server order to.
#
#	var vel = Vector3(ghost.global_transform.origin-ghost.pos).abs()
#	vel = vel.x+vel.z
#	var curr_anim = "idle"
#	if vel > 0.1:
#		curr_anim = "run"
#
#
#	if curr_anim != old_anim:
#		print(str("changing anim: ", curr_anim))
#		anim_node.play(curr_anim)
#
#	old_anim = curr_anim
##
##
