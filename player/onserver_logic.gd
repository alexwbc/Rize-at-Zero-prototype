extends Node
#player-frame on server (PFoS): those are the only actual entities who are
#playing the game. No one but the server can have direct control on their
#status. Player-frame on server have no assets other than serving debug propouse.
#PFoS receive remote input, perform action, server elaborate the result and
#update other clients accordly

#this script is removed in the client packaging.

#based upon Godot kinematic demo (cubio.gd)

#TODO:
	#receive input from remote vehicle
		#walk direction
		#button action
		#button run
#DONE

var onserver
var input_map = [false,false,false,false]
 

var current_status = 0


# Member variables
var g = -9.8
var vel = Vector3()
const MAX_SPEED = 10
const JUMP_SPEED = 7
const ACCEL= 2
const DEACCEL= 9
const MAX_SLOPE_ANGLE = 30


var sync_delay = 0.03
var sync_count = 0

func _physics_process(delta):

	var dir = onserver.dir
	dir.y = 0
	dir = dir.normalized()
	
	vel.y += delta * g
	
	var hvel = vel
	hvel.y = 0
	
	var target = dir * MAX_SPEED
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
	
	hvel = hvel.linear_interpolate(target, accel * delta)
	
	vel.x = hvel.x
	vel.z = hvel.z
	
	#	vel = move_and_slide(vel, Vector3(0,1,0))
	vel = onserver.move_and_slide(vel, Vector3(0,1,0))


	if onserver.is_on_floor() and Input.is_action_pressed("jump"):
		vel.y = JUMP_SPEED
		
	onserver.dir = Vector3(0,0,0)







func _enter_tree():
	assert(get_tree().is_network_server())
	onserver = get_parent()
	if gamestate.player_target_test == null:
		gamestate.player_target_test = onserver


func _ready():
	set_physics_process(true)

func normal_process(delta):
	if onserver.input_map[onserver.input_map_db["fast_travel"]]:
		print("request fast travel")
	if onserver.input_map[onserver.input_map_db["stealth"]]:
		current_status = onserver.status_db["stealth"]
	if onserver.input_map[onserver.input_map_db["scavenge"]]:
		print("request scavenge")
	if onserver.input_map[onserver.input_map_db["jump"]]:
		print("request jump")

func stealth_process(delta):
#	if onserver.input_map[onserver.input_map_db["fast_travel"]]:
#		print("request fast travel")
	if !onserver.input_map[onserver.input_map_db["stealth"]]:
		current_status = onserver.status_db["normal"]
#	if onserver.input_map[onserver.input_map_db["scavenge"]]:
#		print("request scavenge")
#	if onserver.input_map[onserver.input_map_db["jump"]]:
#		print("request jump")


func _process(delta):
	if onserver.message != "":
		print(str("GOT: ", str(onserver.message)))
		onserver.message = ""
	
	if onserver.status == onserver.status_db["normal"]:
		normal_process(delta)
	elif onserver.status == onserver.status_db["stealth"]:
		stealth_process(delta)

	if onserver.input_map != input_map:
		input_map = onserver.input_map.duplicate(true)
#		onserver.input_map = [false,false,false,false]
	

	if sync_count < 0:
		onserver.rset_unreliable("pos", onserver.global_transform.origin)
		sync_count = sync_delay
	else:
		sync_count -= delta
	
	if onserver.status != current_status:
		change_state(current_status)

func change_state(status=null):
	print(str("status change from: ", onserver.status, " to ", status))
	onserver.rset("status", status)


	
	