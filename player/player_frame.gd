extends KinematicBody

#player frame is just what it say: the chase node that will contain

#mode onserver,vehicle,ghost
var mode
var spawn_pos

sync var input_map = [false,false,false,false]
const input_map_db = 	{"fast_travel":0
						,"stealth":1
						,"scavenge":2
						,"jump":3
						}
sync var status = 0
#

const status_db =	{"normal":0#run and jump process
					,"fast_travel":1#running forward, jump while run
					,"stealth":2#walk slow, no jump
					,"scavenge":3#no walk, no jump, fall or hit interrupt status
					}


sync var pos = Vector3()
sync var dir = Vector3()

sync var message = ""

func _physics_process(delta):
	pass
#	print("alive")
#	print(global_transform.origin)

func _enter_tree():
#	pos = global_transform.origin
#	print("enterpos: " +str(pos))
	print(str("placing on: ",spawn_pos))
	pos = spawn_pos
	global_transform.origin = spawn_pos
	if mode == "onserver":
		set_up_onserver()
	elif mode == "vehicle":
		set_up_vehicle()
	elif mode == "ghost":
		set_up_ghost()

	set_physics_process(false)





func set_up_onserver():
	assert(get_tree().is_network_server())
	$yaw.queue_free()
	$logic.set_script(load("res://player/onserver_logic.gd"))
	rset("pos", pos)
func set_up_vehicle():
	$yaw/pitch/camera.make_current()
	print("setting: " +str(name)+" as: "+str(mode) + " on client as vehicle")
	$logic.set_script(load("res://player/vehicle_logic.gd"))
func set_up_ghost():
	print("setting: " +str(name)+" as: "+str(mode) + " on client as ghost")
	$yaw.queue_free()

		
	var clothes = get_node("bodymesh/character/rig/Skeleton/clothes")
	if clothes != null:
		var material = SpatialMaterial.new()
		material.set_blend_mode(2)
		clothes.set_surface_material(0, material)
#	for m in $bodymesh.get_children():
#		var material = m.get_surface_material(0).duplicate()
#		material.set_blend_mode(2)
#		m.set_surface_material(0, material)

	$logic.set_script(load("res://player/ghost_logic.gd"))

#func _process(delta):
#	if get_tree().is_network_server():
#		return
#	print("root input_map: "+str(input_map))