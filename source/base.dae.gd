tool
extends EditorScenePostImport



func post_import(scene):
	print("importbegin")
	assert scene.get_node("pos") != null#spawnpoint node must exist
	var spawnfolder = scene.get_node("pos")
	
	
	for spawn in spawnfolder.get_children():

		var new_spawn = Position3D.new()
		new_spawn.global_transform.origin = spawn.transform.origin
		new_spawn.name = spawn.name
		spawn.queue_free()
		spawnfolder.add_child(new_spawn)
		new_spawn.set_owner(scene)
	print("importend")
	return scene