extends Node
#world logic from client point of view

var world
var swarm_template = preload("res://elements/swarm.tscn")


remote func introduce_swarm(cord):
	print(str("client introducing swarm at: ", cord))
	var new_swarm = swarm_template.instance()
	print(new_swarm)

func _enter_tree():
	print("world logic for client")

